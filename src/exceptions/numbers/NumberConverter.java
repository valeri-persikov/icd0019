package exceptions.numbers;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {


    private Properties properties;


    public NumberConverter(String lang) {
        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);

        properties = new Properties();
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, StandardCharsets.UTF_8);

            properties.load(reader);

        } catch (FileNotFoundException e) {
            throw new MissingLanguageFileException();

        }
        catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException();
        }
        catch (IOException e) {
            throw new RuntimeException();
        }
        try {
            is.close();
        }
        catch (IOException e) {
            throw new RuntimeException();
        }

    }

        public String teenWord (Integer number){
            try {
                if (properties.getProperty(String.valueOf(0)).equals("")) {
                    return null;
                }
            } catch (NullPointerException e) {
                throw new MissingTranslationException();
            }
        String stringNumber = String.valueOf(number);
        if (!properties.containsKey(String.valueOf(number))) {
                return properties.getProperty(stringNumber.substring(stringNumber.length() - 1)) + properties.getProperty("teen");
        }
        return properties.getProperty(String.valueOf(number));
    }

    public String tensWord (Integer number){
        String stringNumber = String.valueOf(number);
        if (!properties.containsKey(String.valueOf(number))) {
            if (number % 10 == 0){
                return properties.getProperty(stringNumber.substring(0, stringNumber.length() - 1))  + properties.getProperty("tens-suffix");
            }
           else if (number % 10 != 0 && !properties.containsKey(String.valueOf(number%100 - number%100%10))) {
                return properties.getProperty(String.valueOf(number%100).substring(0,1))  + properties.getProperty("tens-suffix") + properties.getProperty("tens-after-delimiter") + properties.getProperty(stringNumber.substring(stringNumber.length() - 1));
            }
        }
        return properties.getProperty(String.valueOf(number%100 - number%100%10)) + properties.getProperty("tens-after-delimiter") + properties.getProperty(String.valueOf(number % 10));
    }


    public String numberInWords(Integer number) {
        String stringNumber = String.valueOf(number);
        if (!properties.containsKey(String.valueOf(number))) {
            for (int i = 0; i < stringNumber.length(); i++) {
                int firstDigit = Character.digit(stringNumber.charAt(i), 10);
                String oneHundred = properties.getProperty(String.valueOf(firstDigit)) + properties.getProperty("hundreds-before-delimiter") + properties.getProperty("hundred") + properties.getProperty("hundreds-after-delimiter");

                if (stringNumber.length() == 3) {
                    if ((number % 100) % 10 == 0  && !properties.containsKey(String.valueOf(number % 100))) {
                        return oneHundred  +  properties.getProperty(String.valueOf(number%100).substring(0,1)) + properties.getProperty("tens-suffix");
                    } else if (number % 100 > 20 && !properties.containsKey(String.valueOf(number % 100))){
                        return oneHundred  + tensWord(number);

                    } else if (!properties.containsKey(String.valueOf(number % 100))) {
                        return oneHundred  + teenWord(number);

                    } else if (number % 100 != 0) {
                        return oneHundred +  properties.getProperty(String.valueOf(number % 100));

                    } else {
                        return properties.getProperty(String.valueOf(firstDigit)) + properties.getProperty("hundreds-before-delimiter") + properties.getProperty("hundred");
                    }
                }
                    //TEENS
                    if (firstDigit == 1){
                        return teenWord(number);
                    }
                    //TY
                    if (firstDigit > 1){
                        return tensWord(number);

                }
            }
        }
        return properties.getProperty(String.valueOf(number));
    }

}

