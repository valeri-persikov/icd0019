package exceptions.basic;

public class TryCatchSample {
    public String readDataFrom(Resource resource) {
        String read;
        try {
            resource.open();
            read = resource.read();
        }catch (Exception e) {
            return "someDefaultValue";
        }finally {
            resource.close();
        }
        return read;
    }
}
