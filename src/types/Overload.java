package types;

public class Overload {

    public static void main(String[] args) { add3("22","13");
        System.out.println(add1(1,2));
        System.out.println(add2(1,2));

    }

    public static long add1(long x, long y) {
        System.out.println("Adding longs");
        return x + y;
    }

    public static int add2(int x, int y) {
        System.out.println("Adding integers");
        return x + y;
    }

    public static long add3(String x, String y) {
        System.out.println("Adding numbers from strings");
        return Long.parseLong(x) + Long.parseLong(y);
    }

}
