package types;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {1, 3, -2, 9};

        System.out.println(sum(numbers));
        System.out.println(average(numbers));// 11
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
        System.out.println(squareDigits("ab3c4d5"));
    }

    public static int sum(int[] numbers) {
        Integer sum = 0;
        for (Integer number : numbers){
            sum += number;
        }
        return sum;
    }

    public static double average(int[] numbers) {
        Double avg = 0.0;
        for (Integer number : numbers){
            avg += number;
        }
        return avg / numbers.length;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0){
            return null;
        }
        Integer min = null;

        for (Integer each : integers) {
            if (min == null) {
                min = each;
            }
            if (each < min) {
                min = each;
            }
        }
        return min;
        }

    public static String asString(int[] elements) {
        String result = "";
        for (Integer element :elements) {
            result += element;
            result += ", ";
        }
        if (elements.length == 0){
            return "";
        }
        return result.substring(0,result.length() - 2);
    }

    public static String squareDigits(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++){
            if (Character.isDigit(s.charAt(i))){
                int number = Integer.parseInt(Character.toString(s.charAt(i)));
                result += number * number;
            }
            else{
                result += s.charAt(i);
            }
        }
        return result;
    }
}
