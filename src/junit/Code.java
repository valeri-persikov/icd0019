package junit;

public class Code {

    public static boolean isSpecial(int number) {
        for (int i = 0; i < 4; i++){
            if (number % 11 == i){
                return true;
            }
        }
        return false;

    }

    public static int longestStreak(String input) {
        if (input.length() < 2){
            return input.length();
        }
        int count = 0;
        int highestCount = 0;
        char[] chars = input.toCharArray();
        char last = chars[0];
        for (Character currentChar : chars) {
            if (currentChar == last) {
                count++;
            } else {
                count = 1;
            }
            if (count > highestCount) {
                highestCount = count;
            }
            last = currentChar;
        }
        return highestCount;
    }

    public static Character mode(String input) {
        if (input == null || input.trim().length() == 0){
            return null;
        }
        char[] chars = input.toCharArray();
        int highestCount = 0;
        char mostFrequentChar = 0;
        for (int i = 0; i < input.length(); i++) {
            int count =0;
            for (int j = 0; j < input.length(); j++) {
                if (chars[i] == chars[j]) {
                    count++;
                }
                if (count > highestCount){
                    highestCount = count;
                    mostFrequentChar = chars[j];
                }
            }
        }
        return mostFrequentChar;
    }

    public static int getCharacterCount(String input, char c) {
        char[] chars = input.toCharArray();
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            if (chars[i] == c){
                count++;
            }
        }
        return count;
    }
    public static int[] removeDuplicates(int[] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length; j++) {
                int bubble = 0;
                if (input[i] > input[j]){
                    bubble = input[j];
                    input[i] = input[j];
                    input[j] = bubble;
                }
            }
        }
        int j = 1;
        for (int i = 1; i < input.length; i++) {
            if (input[i] != input[i-1]){
                j++;
            }
        }
        int[] noDup = new int[j];
        noDup[0] = input[0];
        j = 1;
        for (int i = 1; i <input.length; i++) {
            if (input[i] != input[i - 1]) {
                noDup[j++] = input[i];
            }
        }
        return noDup;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int sum = 0;
        for (Integer number: removeDuplicates(integers)){
            sum+= number;
        }
        return sum;
    }

}
