package junit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Tests {

    @Test
    public void equalityExamples() {

    }

    @Test
    public void assertThatAndAssertEqualsExample() {

    }

    @Test
    public void findsSpecialNumbers() {
        assertTrue(Code.isSpecial(0));
        assertTrue(Code.isSpecial(1));
        assertTrue(Code.isSpecial(2));
        assertTrue(Code.isSpecial(3));
        assertTrue(Code.isSpecial(11));
        assertTrue(Code.isSpecial(36));
        assertFalse(Code.isSpecial(4));
        assertFalse(Code.isSpecial(37));
        assertFalse(Code.isSpecial(15));

        // other test cases for isSpecial() method
    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak(""), is(0));
        assertThat(Code.longestStreak("aa"), is(2));
        assertThat(Code.longestStreak("a"), is(1));
        assertThat(Code.longestStreak("abbccc"), is(3));

        // other test cases for longestStreak() method
    }

    @Test
    public void findsModeFromCharactersInString() {

        assertThat(Code.mode(null), is(nullValue()));
        assertThat(Code.mode("acbccbcbb"), is(('c')));
        assertThat(Code.mode("aaabcb"), is('a'));
        // other test cases for mode() method
    }
    @Test
    public void CharacterCount(){
        assertThat(Code.getCharacterCount("acbccbcbb",'c'), is (4));
        assertThat(Code.getCharacterCount("acbccbcbb",'b'), is (4));
        assertThat(Code.getCharacterCount("acbccbcbb",'a'), is (1));
    }



    @Test
    public void removesDuplicates() {

        assertThat(Code.removeDuplicates(arrayOf(1, 1)), is(arrayOf(1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 2)), is(arrayOf(1, 2)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
        assertThat(Code.removeDuplicates(arrayOf(0, 0, 3)), is(arrayOf( 0, 3)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 2)), is(3));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private int[] arrayOf(int... numbers) {
        return numbers;
    }

}
