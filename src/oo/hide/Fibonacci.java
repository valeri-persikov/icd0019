package oo.hide;

public class Fibonacci {
    private int firstNum = 0;
    private int secondNum = 1;

    public int nextValue()
    {
        int sum = firstNum + secondNum;
        secondNum = firstNum;
        firstNum = sum;
        return secondNum;
    }

}
