package oo.hide;


import java.util.Arrays;


public class PointSet {


    private Point[] arrayCapacity;

    public PointSet(int capacity) {
        arrayCapacity = new Point[capacity];


    }

    public PointSet() {
        arrayCapacity = new Point[5];

    }


    public void add(Point point) {
        if (!contains(point)) {
            if (arrayCapacity.length == size()) {
                arrayCapacity = multiplyArray(arrayCapacity);
            } else {
                arrayCapacity[size()] = point;
            }

        }
    }

    @Override
    public boolean equals(Object obj) {


        if (!(obj instanceof PointSet)) {
            return false;
        }

        PointSet other = (PointSet) obj;

        if (((PointSet) obj).size() > 1){
        for (int i = 0; i < ((PointSet) obj).size(); i++) {
            for (int j = 0; j < ((PointSet) obj).size(); j++) {
                if (other.arrayCapacity[i].equals(((PointSet) obj).arrayCapacity[j])) {
                    return true;
                }
            }
            }
        }

        return Arrays.equals(other.arrayCapacity, arrayCapacity);

    }
    @Override
    public String toString() {
        StringBuilder arrayString = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            arrayString.append(String.format("%s, ", arrayCapacity[i]));
            }
        return arrayString.substring(0, arrayString.length() - 2);

    }

    public Point[] multiplyArray(Point[] newArray) {
        int length = newArray.length * 2;
        newArray = new Point[length];
        return newArray;
    }

    public int size() {
        int size = 0;
        for (Point point : arrayCapacity) {
            if (point instanceof Point) {
                size++;
            }
        }
        return size;
    }


    public boolean contains(Point point) {
        for (Point point1 : arrayCapacity) {
            if (point.equals(point1)) {
                return true;
            }
        }
        return false;
    }



    public PointSet subtract(PointSet other) {
        PointSet obj = new PointSet();
        int count = 0;
        for (int i = 0; i < other.size(); i++) {
            count = 0;
            for (int j = 0; j < other.size(); j++) {
                if (arrayCapacity[i].equals(other.arrayCapacity[j])) {
                    break;
                }
                count++;
                if (count == other.size()){
                    obj.add(arrayCapacity[i]);}


            }
        }

        return obj;

    }




    public PointSet intersect (PointSet other) {
        PointSet obj = new PointSet();
        for (int i = 0; i < other.size(); i++) {
            for (int j = 0; j < other.size() ; j++) {
                if (other.arrayCapacity[i].equals(arrayCapacity[j])) {
                        obj.add(arrayCapacity[j]);
                    }
                }
            }
        return obj;
    }
}

