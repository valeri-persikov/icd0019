package oo.hide;

public class Counter {

    private int nStart;
    private int nStep;

    public Counter(int start, int step) {
        this.nStart = start;
        this.nStep = step;

    }

    public int nextValue() {
        int length = nStart;
        nStart += nStep;
        return length;
    }
}

