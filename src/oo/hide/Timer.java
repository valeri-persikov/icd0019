package oo.hide;

public class Timer {

    long start = System.currentTimeMillis();

    public String getPassedTime() {
        double result = System.currentTimeMillis() - start;
        return String.format("%s sek", result /1000);
    }
}
